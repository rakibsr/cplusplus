#include <iostream>
#include <string>

using namespace std;

typedef struct List
{
	char *subject;
	List *next;
	List()
	{
		next=NULL;
		subject=NULL;
	}
};

class Student
{
   public:
      int getRoll();
      Student(int roll );             // simple constructor
      Student(const Student &obj);  // copy constructor
      ~Student();                     // destructor
      void addSubjects();
      void display();
      List *head,*temp;

   private:
      int *roll;
};