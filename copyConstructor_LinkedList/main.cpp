/*
    This file was created before 2009 as a part of learing process.
	It demonstates 
	 - usage of copy constructor
	 - adding link list as a class property
*/

#include"main.h"

Student::Student(int roll)
{
    cout << "Constructor with param is called. Allocating memory for roll ..." << endl;
    this->roll = new int;
    *this->roll = roll;
}

Student::Student(const Student &obj)
{
    cout << "Copy constructor allocating roll." << endl;
    roll = new int;
   *roll = *obj.roll; // copy the value
}

Student::~Student()
{
    cout << "\nFreeing memory!" << endl;
    delete roll;
	temp=head;
   while(temp->next!=NULL)
	{
		delete temp->subject;
		temp=temp->next;
	}
}

int Student::getRoll()
{
    return *roll;
}

void Student::addSubjects()
{
	int i=0;
	head =new List();
	while(i<10)
	{
		List *aNode =new List();
		aNode->subject= new char;
		sprintf(aNode->subject, "Subject Code %d\n", 100+i);
		temp=head;
		while(temp->next!=NULL)
		{
			temp=temp->next;
		}
		temp->next=aNode;
		i++;
	}
}

void Student::display()
{
   cout << "Roll of the student : " << this->getRoll() <<endl;
   temp=head;
   while(temp->next!=NULL)
	{
		temp=temp->next;
		cout<<temp->subject;
	}
}

int main( )
{
   Student Student1(10);
   Student Student2 = Student1; // This calls copy constructor
   Student1.addSubjects();
   Student1.display();

   return 0;
}