// https://en.cppreference.com/w/cpp/language/aggregate_initialization

/*
struct A { int x; int y; int z; };
A a{.y = 2, .x = 1}; // error; designator order does not match declaration order
A b{.x = 1, .z = 2}; // ok, b.y initialized to 0

struct A { int x, y; };
struct B { struct A a; };
struct A a = {.y = 1, .x = 2}; // valid C, invalid C++ (out of order)
int arr[3] = {[1] = 5};        // valid C, invalid C++ (array)
struct B b = {.a.x = 0};       // valid C, invalid C++ (nested)
struct A a = {.x = 1, 2};      // valid C, invalid C++ (mixed)

char a[] = "abc";
// equivalent to char a[4] = {'a', 'b', 'c', '\0'};
 
//  unsigned char b[3] = "abc"; // Error: initializer string too long
unsigned char b[5]{"abc"};
// equivalent to unsigned char b[5] = {'a', 'b', 'c', '\0', '\0'};
*/


#include <iostream>
#include <string>

enum class Animation: int{
  Hide=0,
  Show,
  Flicker
};

struct Icon {
  int id;
  char name[10];
  Animation currentAnim; 
  Animation nextAnim;
  int isActive; 
};

static struct Icon IconList[]= {
    {1, "Offline", Animation::Hide, Animation::Hide, 1},
    {2, "Training", Animation::Hide, Animation::Hide, 1},
	/*The braces around the nested initializer lists may be omitted in aggregate initialization.
	 'name' is a subaggregate containing 10 elements, the 2nd 0 in the initializer is 
	 just used to initialize the 1st element of 'name', then Animation::Hide tries to use to initialize 
	 the 2nd and the 3rd element; but Animation can't convert to char implicitly. Thats why following 
	 aggregate_initialization will give compile error.
	*/
    //{0, 0, Animation::Hide, Animation::Hide, 1}
	/* correct initialization would be */
    {0, {0}, Animation::Hide, Animation::Hide, 1}
};

int main()
{
  std::cout << "Doesn't matter";
}