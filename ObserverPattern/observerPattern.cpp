#include <iostream>
#include <vector>
#include<algorithm>
#include <stdio.h>
#include <string.h>
using namespace std;

//http://arsenmk.blogspot.com/2012/07/simple-implementation-of-observer.html

// Very simple example. Single listener Single Notifier

/*
1. Listener derived from an abstract base IListener
2. Notifier derived from INotifier. INotifier registers IListener(keep references)
3. each time any change occurs, notifier notifies listener by calling listener's common method ie. HandleNotification()
*/

class IListener
{
public:
	virtual void HandleNotification() = 0;
};

class INotifier
{
private:
	IListener *pListener; //3. the notifier keeps reference of listener. Whos to notify?
public:
	virtual void Register(IListener *l)
	{
		pListener = l;
	}
	virtual void UnRegister(IListener *l)
	{
		pListener = NULL;
	}
protected:
	virtual void Notify()// This is private to the child class
	{
		pListener->HandleNotification();
	}
};


class Listener : public IListener //1.all the listerners are derived from an abstract base IListener
{
private:
	virtual void HandleNotification()
	{
		cout << "Listener: I am notified" << endl;
	}
public:
	Listener() { cout << "Listener: obj created" << endl; }
};


class Notifier:public INotifier
{
public:
	void DataReceived()
	{
		cout << "Something happens. Notify listener" << endl;
		Notify();
	}
};

int main()
{
	Notifier n;
	Listener l;
	n.Register(&l);
	n.DataReceived();
	cout << "main()";
	cin.get();
	return 0;
}