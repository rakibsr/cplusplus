#include <iostream>
using namespace std;

#define LOG(x) cout<<x<<"\n";

class Box {

private:
	static Box *pInstance;
	int len;
	Box();
	Box(int k);
public:
	~Box();
	static Box* GetInstance(int k);
	void setLength(int length);
	void getLength();
};