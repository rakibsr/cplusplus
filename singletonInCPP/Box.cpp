#include <Box.h>


Box* Box::pInstance = NULL;

Box::Box()
{
	LOG("Default Constructor()");
}

Box::Box(int k)
{
	len = k;
	LOG("Constructor() with value");
}

Box::~Box()
{
	LOG("Destructor()");
}

Box* Box::GetInstance(int k)
{
	if (NULL == pInstance) {
		LOG(k);
		pInstance = new Box(k);
	}
	return pInstance;
}

void Box::setLength(int length)
{
	len = length;
}

void Box::getLength()
{
	LOG(len);
}

