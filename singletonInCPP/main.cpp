/* Created in 2014.
Single pattern in C++
Author: rakibdana@gmail.com / rakibsr@gmail.com
*/

#include <iostream>
#include <string>
#include <Box.h>

using namespace std;

int main()
{
	Box *box = box->GetInstance(15);
	box->getLength();
	box->setLength(1005);
	box->getLength();

	//box2 points to box, same object
	Box *box2 = box2->GetInstance(11);
	box2->getLength();

	delete box;

	// this will crash
	//delete box2;
	return 0;
}