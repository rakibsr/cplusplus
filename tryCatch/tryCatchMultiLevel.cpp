#include <iostream>
#include <exception>
#include <stdexcept>
using namespace std;
int i = 3;
class A : public runtime_error {
public: A() : runtime_error("?") {}
};
class B : public logic_error {
public: B() : logic_error("!") {}
};
void f() {
 i++; // incremented
 throw A();
 i++; // won't be executed
}
void g() {
 try { 
	f();
	throw B();
 } catch(A &ob) {  // exception is caught from f()
    cout << "run-time error" << endl; // printed
	throw B();
 }
}
int main() {
 try { 
	g(); 
	i++;
 } catch(logic_error &l) { 
	i++; // incremented
	cout << "logic error!" << endl; //printed
 } catch(...) { 
	cout << "unknown exception!" << endl;
 }
 cout << i << endl; // 5
 return 0;
}